﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace MiniPdfWriter
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog.Filter = "PDF Files|*.pdf";
            saveFileDialog.ShowDialog();
            if (saveFileDialog != null)
            {
                Paragraph p = new Paragraph(richTextBox.Text);
                string path = saveFileDialog.FileName;
                path.Replace('\\', '/');

                Document doc1 = new Document();
                PdfWriter.GetInstance(doc1, new FileStream(path, FileMode.Create));

                doc1.Open();
                doc1.Add(p);
                doc1.Close();

                Process.Start(path);
            }
            richTextBox.Text = "";
            richTextBox.Visible = false;
            panel.Visible = false;

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            richTextBox.Visible = false;
            panel.Visible = false;
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox.Text = "";
            panel.Visible = true;
            richTextBox.Visible = true;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panel.Visible = false;
            richTextBox.Visible = false;
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox1 about = new AboutBox1();
            about.StartPosition = FormStartPosition.CenterScreen;
            about.Visible = true;
        }
    }
}
